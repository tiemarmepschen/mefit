export enum StorageKeys {
    User = "userObject",
    KeycloakId = "keycloakId",
    UserName = "user",
    Profile = "profileObject",
    UserId = "userId"
}