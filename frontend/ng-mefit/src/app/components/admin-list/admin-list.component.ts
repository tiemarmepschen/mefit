import { Component, EventEmitter, Input, Output } from '@angular/core';
import { User } from 'src/app/models/user.model';
import { UserService } from 'src/app/services/user.service';

@Component({
  selector: 'app-admin-list',
  templateUrl: './admin-list.component.html',
  styleUrls: ['./admin-list.component.css']
})
export class AdminListComponent {

  @Input() user?: User;

  @Output() userUpdated: EventEmitter<number> = new EventEmitter();

  constructor(
    private readonly userService: UserService
    ) { }

  public handleAdminAcceptButton(id: number) {
    // update user status from admin pending to admin
    this.userService.updateUserAdminAccepted(id)
    // remove user from admin-list
    this.userUpdated.emit(id);
  }

  public handleAdminRejectButton(id: number) {
    // update user status from admin pending to user
    this.userService.updateUserAdminRejected(id)
    // remove user from admin-list
    this.userUpdated.emit(id);
  }
}
