import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from 'src/environments/environment';
import { StorageKeys } from '../enums/storage-keys.enum';
import { Profile, User } from '../models/user.model';

const { API_URL } = environment;

@Injectable({
  providedIn: 'root',
})
export class ProfileService {

  constructor(private readonly http: HttpClient) {}

  // get user
  get user(): User | undefined{
    let userObject = JSON.parse(sessionStorage.getItem(StorageKeys.User)!)
    return userObject
  }

  // get profile
  get profile(): Profile | undefined{
    let profileObject = JSON.parse(sessionStorage.getItem(StorageKeys.Profile)!)
    return profileObject;
  }
  
  public updateProfile(currentGoalId: number): any {

    let profileObject = JSON.parse(sessionStorage.getItem(StorageKeys.Profile)!)
    profileObject.currentGoalId = currentGoalId;

    this.http.put<Profile>(`${API_URL}/Profile/${profileObject.id}`, profileObject)
    .subscribe({
      next: (profile: Profile) => {
        this.getProfileById(profileObject.id);
      },
      error: (error: HttpErrorResponse) => {
        throw new Error(error.message);
      }
    })
  }

  // get profile by id
  public getProfileById(id: number): any{
    this.http.get<Profile>(`${API_URL}/Profile/${id}`)
    .subscribe({
      next: (profile: Profile) => {
          sessionStorage.setItem(StorageKeys.Profile, JSON.stringify(profile));
      },
      error: (error: HttpErrorResponse) => {
        throw new Error(error.message);
      },
    });
  }

  public deleteGoal(id: number, userId: number): any {
    const body = {
      id: id,
      currentGoalId: null,
      goalId: null,
      userId: userId
    }
    this.http.put(`${API_URL}/Profile/${id}`, body).subscribe({
      next: (profile: any) => {
        // update profile in sessionStorage
        this.getProfileById(id);
      }
    });
  }
}
